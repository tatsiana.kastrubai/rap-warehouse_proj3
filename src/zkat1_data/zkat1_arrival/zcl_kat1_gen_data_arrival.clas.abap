CLASS zcl_kat1_gen_data_arrival DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    INTERFACES if_oo_adt_classrun.
  PROTECTED SECTION.
  PRIVATE SECTION.
    DATA out TYPE REF TO object.
    METHODS fill_arrival
      IMPORTING
        out TYPE REF TO if_oo_adt_classrun_out.
ENDCLASS.



CLASS ZCL_KAT1_GEN_DATA_ARRIVAL IMPLEMENTATION.


  METHOD fill_arrival.
    DATA: lt_header     TYPE TABLE OF zkat1_d_arrvh_a,
          lt_item       TYPE TABLE OF zkat1_d_arrvit_a,
          lt_matr       TYPE TABLE OF zkat1_d_matr_t,
          lv_created_at TYPE timestampl.

    lt_matr = VALUE #(
                ( short_name ='Sock absorber PN-24'  )
                ( short_name ='Oxygen, 2L' )
                ( short_name ='Oil filter FR-171' )
                ( short_name ='Oil filter FR-170C' )
                ( short_name ='Oil pan gasket' )
                ( short_name ='Palitra RAL'  )
                ( short_name ='Cylinder liner kit 18' )
                ( short_name ='Wallpaper Ui32655-w' )
                ( short_name ='Container, 15L'  )
                ( short_name ='Flint for Turbina RE-127' )
                ( short_name ='Test RE-12766' )
                ( short_name ='Small hunziker shot blasting (546002)' )
                ( short_name ='Al-uid2-a90 leak test' )
                ( short_name ='Tempering furnace' )
                ( short_name ='Punched press' )
                ( short_name ='Wigen elevator' )
                ( short_name ='Daw-80 robot fanuc' )
                ( short_name ='Drum separator casting' )
                ( short_name ='Rauch portable oven' )
                ( short_name ='Portable oven (Germany)' )
                ( short_name ='With.hot chamber temp.' )
                ( short_name ='Moreto plastic mill' )
                ( short_name ='Hb-therm desuperheater' )
                ( short_name ='Moditec plastic mill' )
                ( short_name ='Regoplas desuperheater' )
                ( short_name ='Conveyor belt' )
                ( short_name ='Mobile dehumidifier' )
                ( short_name ='Regloplas desuperheater' )
                ( short_name ='Hb-thern 2ªm desuperheater' )
                ( short_name ='Canon printer' )
                ( short_name ='Ricoh multifunction' )
                ( short_name ='Acct. pieces+platform' )
                ( short_name ='Daimler latch' )
                ( short_name ='Sub-bassy housing left + sealing' )
                ( short_name ='Subassy housing right + sealing' )
                ( short_name ='Bassy lever + spring' )
                ( short_name ='Sub-bassy lever + spring left' )
                ( short_name ='Key sub-assembly toyota' )
                ( short_name ='Subassy sliders' )
                ( short_name ='Key sub-assembly peugeot' )
                ( short_name ='Key sub-assembly citroen' )

          ).

    LOOP AT lt_matr ASSIGNING FIELD-SYMBOL(<fs_matr>).
      GET TIME STAMP FIELD lv_created_at.
      <fs_matr>-spras = 'E'.
      <fs_matr>-matr_id = | { 17000 + sy-tabix } |.
      <fs_matr>-last_changed_at = lv_created_at.
      WAIT UP TO 1 SECONDS.
    ENDLOOP.

    DELETE FROM zkat1_d_matr_t.
    INSERT zkat1_d_matr_t FROM TABLE @lt_matr.

    IF sy-subrc = 0.
      out->write( |{ sy-dbcnt } records of Matr are inserted!| ).
    ELSE.
      out->write( 'error' ).
    ENDIF.

*    lt_header = VALUE #( ( arrv_id ='a00010' doct_id ='ACT' supl_id ='234990001' arrv_date = '20221223' arrv_status ='1'  )
*                         ( arrv_id ='a00014' doct_id ='INV' supl_id ='234990001' arrv_date = '20221227' arrv_status ='1'  )
*                         ( arrv_id ='a00011' doct_id ='ACT' supl_id ='470567701' arrv_date = '20221225' arrv_status ='1'  )
*                         ( arrv_id ='a00013' doct_id ='INV' supl_id ='470567701' arrv_date = '20221227' arrv_status ='2'  ) ).

lt_header = VALUE #(
  ( CLIENT ='100' ARRV_UUID ='A6CE48C494271EEDA3D73809AE80BED1' WRH_UUID ='1EACE85DF05C1EEDA39E341A50E65E51' ARRV_ID ='A10004' SUPL_ID ='234990001' DOCT_ID ='ORD' ARRV_DATE ='20221229' POST_DATE ='20221229' ARRV_STATUS ='3' CREATED_BY ='' CREATED_AT =
'20221229013652.2294560 ' LAST_CHANGED_BY ='' LAST_CHANGED_AT ='20221229013652.2294560 '  )
 ( CLIENT ='100' ARRV_UUID ='9E79B67E7C831EEDA48F07900BD042C9' WRH_UUID ='1EACE85DF05C1EEDA39E341A50E65E51' ARRV_ID ='D00027' SUPL_ID ='234990001' DOCT_ID ='ACT' ARRV_DATE ='20230102' POST_DATE ='20230111' ARRV_STATUS ='2' CREATED_BY ='' CREATED_AT =
'20230102013652.2294560 ' LAST_CHANGED_BY ='' LAST_CHANGED_AT ='20230111013652.2294560 '  )
 ( CLIENT ='100' ARRV_UUID ='9E79B67E7C831EEDA48F5524DCEF437E' WRH_UUID ='1EACE85DF05C1EEDA39E341A50E65E51' ARRV_ID ='F30090' SUPL_ID ='234990001' DOCT_ID ='INV' ARRV_DATE ='20221231' POST_DATE ='00000000' ARRV_STATUS ='1' CREATED_BY ='' CREATED_AT =
'20221231013652.2294560 ' LAST_CHANGED_BY ='' LAST_CHANGED_AT ='0.0000000 '  )
 ( CLIENT ='100' ARRV_UUID ='86272E52B1EF1EDDA4910FB3FBEA971B' WRH_UUID ='1EACE85DF05C1EEDA39E341A50E5DE51' ARRV_ID ='A00002' SUPL_ID ='960023373' DOCT_ID ='INV' ARRV_DATE ='20221227' POST_DATE ='20230101' ARRV_STATUS ='2' CREATED_BY ='' CREATED_AT =
'20221227013652.2294560 ' LAST_CHANGED_BY ='' LAST_CHANGED_AT ='20230101013652.2294560 '  )
 ( CLIENT ='100' ARRV_UUID ='86272E52B1EF1EEDA486FA69CF8D18D6' WRH_UUID ='A6CE48C494271EEDA39E7F73FDE31C68' ARRV_ID ='A30001' SUPL_ID ='470567701' DOCT_ID ='INV' ARRV_DATE ='20221226' POST_DATE ='00000000' ARRV_STATUS ='1' CREATED_BY ='' CREATED_AT =
'20221226013652.2294560 ' LAST_CHANGED_BY ='' LAST_CHANGED_AT ='0.0000000 '  )
 ( CLIENT ='100' ARRV_UUID ='A6CE48C494271EDDA3AB65D4F975FD21' WRH_UUID ='A6CE48C494271EEDA39E7F73FDE31C68' ARRV_ID ='A45601' SUPL_ID ='470567701' DOCT_ID ='ACT' ARRV_DATE ='20221220' POST_DATE ='20230102' ARRV_STATUS ='2' CREATED_BY ='' CREATED_AT =
'20221220013652.2294560 ' LAST_CHANGED_BY ='' LAST_CHANGED_AT ='20230102013652.2294560 '  )
 ( CLIENT ='100' ARRV_UUID ='9E79B67E7C831EEDA4AA85AF3B655134' WRH_UUID ='A6CE48C494271EEDA39E7F73FDE31C68' ARRV_ID ='A3001' SUPL_ID ='960023373' DOCT_ID ='ACT' ARRV_DATE ='20230102' POST_DATE ='00000000' ARRV_STATUS ='1' CREATED_BY ='' CREATED_AT =
'20230102013652.2294560 ' LAST_CHANGED_BY ='' LAST_CHANGED_AT ='0.0000000 '  ) ).

*    LOOP AT lt_header ASSIGNING FIELD-SYMBOL(<fs_header>).
*      GET TIME STAMP FIELD lv_created_at.
*      <fs_header>-arrv_uuid = cl_system_uuid=>if_system_uuid_static~create_uuid_x16(  ).

*      <fs_header>-wrh_id = 'wrh001'.
*      <fs_header>-created_at = lv_created_at.
*      <fs_header>-last_changed_at = lv_created_at.
*      WAIT UP TO 1 SECONDS.
*    ENDLOOP.

    DELETE FROM zkat1_d_arrvh_a.
    INSERT zkat1_d_arrvh_a FROM TABLE @lt_header.

    IF sy-subrc = 0.
      out->write( |{ sy-dbcnt } records of Header are inserted!| ).
    ELSE.
      out->write( 'error' ).
    ENDIF.


    lt_item = VALUE #( ( CLIENT ='100' ITEM_UUID ='86272E52B1EF1EDDA4AA0AF22F431D89' ITEM_ID ='10002' ARRV_UUID ='86272E52B1EF1EDDA4910FB3FBEA971B' WRH_UUID ='1EACE85DF05C1EEDA39E341A50E5DE51' MATR_ID ='17017' UNIT ='ST' AMOUNT ='1.000 ' CURR ='EUR'
PRICE ='243.00 ' MTLGR_ID ='' U_BASE ='' LAST_CHANGED_BY ='' LAST_CHANGED_AT ='20230111013652.2294560 '  )
 ( CLIENT ='100' ITEM_UUID ='9E79B67E7C831EEDA4A9EB5A2C9A4F56' ITEM_ID ='10001' ARRV_UUID ='86272E52B1EF1EDDA4910FB3FBEA971B' WRH_UUID ='1EACE85DF05C1EEDA39E341A50E5DE51' MATR_ID ='17039' UNIT ='ST' AMOUNT ='2.000 ' CURR ='USD' PRICE ='35.00 ' MTLGR_ID =
'' U_BASE ='' LAST_CHANGED_BY ='' LAST_CHANGED_AT ='20230111013652.2294560 '  )
 ( CLIENT ='100' ITEM_UUID ='86272E52B1EF1EDDA4AA1A70CDEEFD98' ITEM_ID ='10003' ARRV_UUID ='9E79B67E7C831EEDA48F07900BD042C9' WRH_UUID ='1EACE85DF05C1EEDA39E341A50E65E51' MATR_ID ='17011' UNIT ='ST' AMOUNT ='1.000 ' CURR ='USD' PRICE ='346.00 ' MTLGR_ID
='' U_BASE ='' LAST_CHANGED_BY ='' LAST_CHANGED_AT ='20230111014032.7299520 '  )
 ( CLIENT ='100' ITEM_UUID ='9E79B67E7C831EEDA4AA20CB57504FA4' ITEM_ID ='10004' ARRV_UUID ='9E79B67E7C831EEDA48F07900BD042C9' WRH_UUID ='1EACE85DF05C1EEDA39E341A50E65E51' MATR_ID ='17013' UNIT ='ST' AMOUNT ='1.000 ' CURR ='USD' PRICE ='17.00 ' MTLGR_ID =
'' U_BASE ='' LAST_CHANGED_BY ='' LAST_CHANGED_AT ='20230111014139.6488390 '  )
 ( CLIENT ='100' ITEM_UUID ='86272E52B1EF1EDDA4AA2782DD9D1DE9' ITEM_ID ='10005' ARRV_UUID ='9E79B67E7C831EEDA48F5524DCEF437E' WRH_UUID ='1EACE85DF05C1EEDA39E341A50E65E51' MATR_ID ='17042' UNIT ='ST' AMOUNT ='4.000 ' CURR ='BYN' PRICE ='72.50 ' MTLGR_ID =
'' U_BASE ='' LAST_CHANGED_BY ='' LAST_CHANGED_AT ='20230111014343.1660570 '  )
 ( CLIENT ='100' ITEM_UUID ='86272E52B1EF1EDDA4AA2E3C0C9A1DFB' ITEM_ID ='10006' ARRV_UUID ='86272E52B1EF1EEDA486FA69CF8D18D6' WRH_UUID ='A6CE48C494271EEDA39E7F73FDE31C68' MATR_ID ='17003' UNIT ='ST' AMOUNT ='3.000 ' CURR ='BYN' PRICE ='32.25 ' MTLGR_ID =
'' U_BASE ='' LAST_CHANGED_BY ='' LAST_CHANGED_AT ='20230111014510.1577190 '  )
 ( CLIENT ='100' ITEM_UUID ='86272E52B1EF1EDDA4AA39F0A085DE10' ITEM_ID ='10007' ARRV_UUID ='A6CE48C494271EDDA3AB65D4F975FD21' WRH_UUID ='A6CE48C494271EEDA39E7F73FDE31C68' MATR_ID ='17001' UNIT ='ST' AMOUNT ='1.000 ' CURR ='EUR' PRICE ='74.50 ' MTLGR_ID =
'' U_BASE ='' LAST_CHANGED_BY ='' LAST_CHANGED_AT ='20230111014802.1195440 '  )
 ( CLIENT ='100' ITEM_UUID ='9E79B67E7C831EEDA4AA41A5E4183009' ITEM_ID ='10008' ARRV_UUID ='A6CE48C494271EDDA3AB65D4F975FD21' WRH_UUID ='A6CE48C494271EEDA39E7F73FDE31C68' MATR_ID ='17002' UNIT ='ST' AMOUNT ='1.000 ' CURR ='EUR' PRICE ='430.00 ' MTLGR_ID
='' U_BASE ='' LAST_CHANGED_BY ='' LAST_CHANGED_AT ='20230111014920.0267530 '  )
 ( CLIENT ='100' ITEM_UUID ='86272E52B1EF1EDDA4AA862CC55E1F4A' ITEM_ID ='10001' ARRV_UUID ='9E79B67E7C831EEDA4AA85AF3B655134' WRH_UUID ='A6CE48C494271EEDA39E7F73FDE31C68' MATR_ID ='17034' UNIT ='ST' AMOUNT ='4.000 ' CURR ='USD' PRICE ='57.50 ' MTLGR_ID =
'' U_BASE ='' LAST_CHANGED_BY ='' LAST_CHANGED_AT ='20230111020518.6026690 '  )
 ( CLIENT ='100' ITEM_UUID ='9E79B67E7C831EEDA4AA1284A1A8CF85' ITEM_ID ='10003' ARRV_UUID ='A6CE48C494271EEDA3D73809AE80BED1' WRH_UUID ='1EACE85DF05C1EEDA39E341A50E65E51' MATR_ID ='17029' UNIT ='KG' AMOUNT ='10.000 ' CURR ='AMD' PRICE ='15.00 ' MTLGR_ID
='' U_BASE ='' LAST_CHANGED_BY ='' LAST_CHANGED_AT ='20230111060121.2482600 '  )
  ).


*    LOOP AT lt_item ASSIGNING FIELD-SYMBOL(<fs_item>).
*        <fs_item>-item_uuid = cl_system_uuid=>if_system_uuid_static~create_uuid_x16(  ).
*        <fs_item>-matr_id = | { 17000 + 2 * sy-tabix } |.
*    ENDLOOP.

    DELETE FROM zkat1_d_arrvit_a.
    INSERT zkat1_d_arrvit_a FROM TABLE @lt_item.


    IF sy-subrc = 0.
      out->write( |{ sy-dbcnt } records of Item are inserted!| ).
    ELSE.
      out->write( 'error' ).
    ENDIF.

  ENDMETHOD.


  METHOD if_oo_adt_classrun~main.
    me->fill_arrival( out ).
  ENDMETHOD.
ENDCLASS.
