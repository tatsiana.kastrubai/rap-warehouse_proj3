@EndUserText.label: 'Arrival Count int view'
@AccessControl.authorizationCheck: #NOT_REQUIRED

define view entity ZKAT1_I_ARRV_COUNT_BY_WRH
  as select from zkat1_d_arrvh_a as ArrivalCount
{
  key wrh_uuid                    as WrhUUID,
      arrv_status                 as StatCode,
      count( distinct arrv_uuid ) as PendingProc
}
group by
  wrh_uuid,
  arrv_status
