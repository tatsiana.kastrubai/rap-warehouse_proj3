@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Arrival Item int view'
define view entity ZKAT1_I_ARRIVALITEMS
  as select from zkat1_d_arrvit_a as ArrvItems
  association     to parent ZKAT1_I_ARRIVAL as _Arrival  on $projection.ArrvUUID = _Arrival.ArrvUUID
  association [1] to ZKAT1_I_WRHOUS         as _Wrhous   on $projection.WrhUUID = _Wrhous.WrhUUID
  /*+[hideWarning] { "IDS" : [ "CARDINALITY_CHECK" ] }  */
  association [1] to ZKAT1_I_MATERIAL       as _MatrText on $projection.MatrID = _MatrText.MatrID
  /*+[hideWarning] { "IDS" : [ "CARDINALITY_CHECK" ] } */
  association [1] to ZKAT1_I_MTGROUP        as _MtlGroup on $projection.MtlgrCode = _MtlGroup.MtlGrCode
{
  key item_uuid                                                           as ItemUUID,
      item_id                                                             as ItemID,
      arrv_uuid                                                           as ArrvUUID,
      wrh_uuid                                                            as WrhUUID,
      matr_id                                                             as MatrID,
      //      _MatrText.Material,
      unit                                                                as Unit,
      amount                                                              as Amount,
      curr                                                                as Curr,
      price                                                               as Price,
      @Semantics.amount.currencyCode: 'Curr'
      cast(amount as abap.dec( 10, 2 )) * cast(price as abap.dec( 10, 2)) as Total,
      //      Currency Conversion
      //      @Semantics.amount.currencyCode: 'Curr'
      //      currency_conversion(
      //      amount => cast(amount * price as abap.dec( 10, 2 )),
      //      source_currency => Curr,
      //      round => 'X',
      //      target_currency => cast('USD' as abap.cuky( 5 )),
      //      exchange_rate_date => cast('20230111' as abap.dats),
      //      error_handling => 'SET_TO_NULL' )                         as PriceInUSD,

      mtlgr_id                                                            as MtlgrCode,
      u_base                                                              as UnitBase,
      last_changed_by                                                     as LastChangedBy,
      @Semantics.systemDateTime.localInstanceLastChangedAt: true
      last_changed_at                                                     as LastChangedAt,

      _Arrival,
      _Wrhous,
      _MatrText,
      _MtlGroup
}
