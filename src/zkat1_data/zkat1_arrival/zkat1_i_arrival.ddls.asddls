@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Arrival heder int view'
define view entity ZKAT1_I_ARRIVAL
//  with parameters
//    @Environment.systemField: #SYSTEM_LANGUAGE
//    p_langu : spras 
  as select from zkat1_d_arrvh_a as Arrival
  association to parent ZKAT1_I_WRHOUS       as _Wrhous   on  $projection.WrhUUID = _Wrhous.WrhUUID
  composition [0..*] of ZKAT1_I_ARRIVALITEMS as _ArrvItems
  /*+[hideWarning] { "IDS" : [ "CARDINALITY_CHECK" ] } */
  association to ZKAT1_I_STATUS              as _Status   on  $projection.StatCode = _Status.StatCode
//                                                         and $projection.Language = _Status.Spras
  /*+[hideWarning] { "IDS" : [ "CARDINALITY_CHECK" ] } */
  association to ZKAT1_I_COMPANY             as _Company  on  $projection.SuplId   = _Company.Payer
  /*+[hideWarning] { "IDS" : [ "CARDINALITY_CHECK" ] } */
  association to ZKAT1_I_DOCTYPE             as _Document on  $projection.DoctID   = _Document.DocCode 
{
  key arrv_uuid       as ArrvUUID,
      wrh_uuid        as WrhUUID,
      arrv_id         as ArrvID,
      doct_id         as DoctID,
      supl_id         as SuplId,
      @Consumption.filter.selectionType: #INTERVAL
      arrv_date       as ArrvDate,
      post_date       as PostDate,
      arrv_status     as StatCode,
      case _Status.StatCode
        when '1'  then 0    -- 'expected'    | 0: unknown
        when '2'  then 3    -- 'credited'    | 3: green colour
        when '3'  then 1    -- 'returned'    | 1: red colour
        else 2              -- 'unknown'     | 2: yellow colour
      end             as StatusCriticality,
      created_by      as CreateBy,
      created_at      as CreateAt,
      last_changed_by as LastChangedBy,
      last_changed_at as LastChangedAt,
//      $parameters.p_langu as Language,

      _ArrvItems,
      _Wrhous,
      _Status,
      _Company,
      _Document
}
