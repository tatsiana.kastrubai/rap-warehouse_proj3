CLASS lhc_arritems DEFINITION INHERITING FROM cl_abap_behavior_handler.
  PRIVATE SECTION.
    METHODS calculateitemid FOR DETERMINE ON SAVE
      IMPORTING keys FOR  ArrvItems~calculateItemId .
ENDCLASS.

CLASS lhc_arritems IMPLEMENTATION.
  METHOD calculateitemid.
    " check if ID is already filled
    READ ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
      ENTITY ArrvItems
        FIELDS ( WrhUUID ArrvUUID ItemId ) WITH CORRESPONDING #( keys )
      RESULT DATA(lt_arrvitems).

    " anything left ?
    CHECK lt_arrvitems IS NOT INITIAL.

    READ TABLE lt_arrvitems INTO DATA(ls_item) INDEX 1.

    " Select max wrhous ID
    SELECT SINGLE
        FROM  zkat1_d_arrvit_a
        FIELDS MAX( item_id ) AS itemID
        WHERE wrh_uuid = @ls_item-WrhUUID AND arrv_uuid = @ls_item-ArrvUUID
        INTO @DATA(lv_max_itemid).

    IF lv_max_itemid IS INITIAL OR lv_max_itemid < 10000.
      lv_max_itemid = 10000.  " Starting number
    ENDIF.

    " Set the wrhous ID
    MODIFY ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
    ENTITY ArrvItems
      UPDATE
        FROM VALUE #( FOR item IN lt_arrvitems INDEX INTO i (
          %tky              = item-%tky
          ItemID             = lv_max_itemid + 1
          %control-ItemID = if_abap_behv=>mk-on ) )
    REPORTED DATA(update_reported).

    reported = CORRESPONDING #( DEEP update_reported ).
  ENDMETHOD.
ENDCLASS.
