@EndUserText.label: 'Manerial int view'
@AccessControl.authorizationCheck: #NOT_REQUIRED
define view entity ZKAT1_I_MATERIAL
  as select from zkat1_d_matr_t
{
  key spras      as Spras,
  key matr_id    as MatrID,
      short_name as Material
}
where
  spras = $session.system_language
