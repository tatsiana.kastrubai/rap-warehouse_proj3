@EndUserText.label: 'ArrivalItems projection view'
@AccessControl.authorizationCheck: #NOT_REQUIRED
@Search.searchable: true

@Metadata.allowExtensions: true
define view entity ZKAT1_C_ARRIVALITEMS
  as projection on ZKAT1_I_ARRIVALITEMS
{
  key ItemUUID,
      @Search.defaultSearchElement: true
      @Search.fuzzinessThreshold: 0.90
      ItemID,
      ArrvUUID,
      WrhUUID,
      @Consumption.valueHelpDefinition: [{ entity: { name: 'ZKAT1_I_MATERIAL', element: 'MatrID'} }]
      @ObjectModel.text.element: ['Material']
      @Search.defaultSearchElement: true
      MatrID,
      _MatrText.Material,
      Unit,
      Amount,
      @Semantics.amount.currencyCode: 'Curr'
      Price,
      @Consumption.valueHelpDefinition: [{ entity: { name: 'I_Currency', element: 'Currency'} } ]
      Curr,
      @Semantics.amount.currencyCode: 'Curr'
      Total,
      @Consumption.valueHelpDefinition: [{ entity: { name: 'ZKAT1_I_MTGROUP', element: 'MtlGrCode'} }]
      @ObjectModel.text.element: ['MtlGrCodeDesc']
      @Search.defaultSearchElement: true
      MtlgrCode,
      _MtlGroup.MtlGrCodeDesc,
      UnitBase,
      LastChangedAt,

      _Arrival : redirected to parent ZKAT1_C_ARRIVAL,
      _Wrhous : redirected to ZKAT1_C_WRHOUS,
      _MatrText,
      _MtlGroup

}
