CLASS lhc_arrival DEFINITION INHERITING FROM cl_abap_behavior_handler.
  PRIVATE SECTION.

    CONSTANTS:
      BEGIN OF arrival_status,
        expected TYPE c LENGTH 1  VALUE '1',
        credited TYPE c LENGTH 1  VALUE '2',
        returns  TYPE c LENGTH 1  VALUE '3',
      END OF arrival_status.

    METHODS setCreditedStatus FOR MODIFY
      IMPORTING keys FOR ACTION  Arrival~creditedStatus RESULT result.
    METHODS cancelCredStatus FOR MODIFY
      IMPORTING keys FOR ACTION  Arrival~cancelCredStatus RESULT result.
    METHODS setReturnsStatus FOR MODIFY
      IMPORTING keys FOR ACTION  Arrival~returnsStatus RESULT result.
    METHODS get_features FOR FEATURES
      IMPORTING keys REQUEST requested_features FOR Arrival RESULT result.

    METHODS validateArrvNumber FOR VALIDATE ON SAVE
      IMPORTING keys FOR Arrival~validateArrvNumber.

    METHODS assignCreditedInfo FOR DETERMINE ON SAVE
      IMPORTING keys FOR Arrival~assignCreditedInfo.

    METHODS checkArrivalsValid FOR DETERMINE ON SAVE
      IMPORTING keys FOR Arrival~checkArrivalsValid.

    METHODS putInStorage FOR MODIFY
      IMPORTING keys FOR ACTION Arrival~putInStorage.

ENDCLASS.

CLASS lhc_arrival IMPLEMENTATION.

  METHOD validateArrvNumber.

    READ ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
      ENTITY Arrival BY \_Wrhous
        FIELDS ( WrhUUID ) WITH CORRESPONDING #( keys )
      RESULT DATA(wrh_data).

    SELECT FROM zkat1_d_arrvh_a FIELDS arrv_id
        INTO TABLE @DATA(gt_arrvh).

    LOOP AT wrh_data INTO DATA(wrh).
      READ ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
        ENTITY Wrhous BY \_Arrival
          FIELDS ( ArrvId ArrvDate DoctID SuplId )
        WITH VALUE #( ( %tky = wrh-%tky ) )
        RESULT DATA(lt_arrivals).

      LOOP AT lt_arrivals INTO DATA(warrvh).
        IF line_exists( gt_arrvh[ arrv_id = warrvh-ArrvID ] ).
*          APPEND VALUE #(  %tky = warrvh-%tky ) TO failed.

          APPEND VALUE #(  %tky        = warrvh-%tky
                           %state_area = 'VALIDATE_ARRVNUMBER'
                           %msg        = NEW zcm_kat1_msg(
                                             severity   = if_abap_behv_message=>severity-error
                                             textid     = zcm_kat1_msg=>number_duplicate
                                             arrvnumber = warrvh-ArrvID )
                           %element-ArrvID = if_abap_behv=>mk-on )
            TO reported-arrival.
        ENDIF.
      ENDLOOP.
    ENDLOOP.
  ENDMETHOD.

  METHOD assignCreditedInfo.

    READ ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
    ENTITY Arrival
      FIELDS ( ArrvID )
      WITH CORRESPONDING #( keys )
      RESULT DATA(lt_arrivals)
      FAILED DATA(read_failed).

    READ TABLE lt_arrivals INTO DATA(ls_arrival) INDEX 1.

    MODIFY ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
    ENTITY Arrival
      UPDATE FIELDS ( StatCode )
      WITH VALUE #( FOR ls_arrv IN lt_arrivals
      ( %key = ls_arrv-%key
        StatCode = arrival_status-expected ) )
    REPORTED DATA(modify_reported).

    reported = CORRESPONDING #( DEEP modify_reported ).

  ENDMETHOD.

  METHOD checkArrivalsValid.

    DATA permission_request TYPE STRUCTURE FOR PERMISSIONS REQUEST zkat1_i_arrival.
    DATA reported_arrv_li LIKE LINE OF reported-arrival.

    DATA(description_permission_request) = CAST cl_abap_structdescr( cl_abap_typedescr=>describe_by_data_ref( REF #( permission_request-%field ) ) ).
    DATA(components_permission_request) = description_permission_request->get_components(  ).

    LOOP AT components_permission_request INTO DATA(component_permission_request).
      permission_request-%field-(component_permission_request-name) = if_abap_behv=>mk-on.
    ENDLOOP.

    " Get current field values
    READ ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
    ENTITY Arrival
      ALL FIELDS
      WITH CORRESPONDING #( keys )
      RESULT DATA(arrivals).

    LOOP AT arrivals INTO DATA(ls_arrv).

      GET PERMISSIONS ONLY INSTANCE FEATURES ENTITY zkat1_i_arrival
                FROM VALUE #( ( ArrvUUID = ls_arrv-ArrvUUID ) )
                REQUEST permission_request
                RESULT DATA(permission_result)
                FAILED DATA(failed_permission_result)
                REPORTED DATA(reported_permission_result).

      DATA(is_empty) = abap_false.
      LOOP AT components_permission_request INTO component_permission_request.

        IF permission_result-global-%field-(component_permission_request-name) = if_abap_behv=>fc-f-mandatory AND
           ls_arrv-(component_permission_request-name) IS INITIAL.

*          APPEND VALUE #( %tky = ls_arrv-%tky ) TO failed-.

          CLEAR reported_arrv_li.
          reported_arrv_li-%tky = ls_arrv-%tky.
          reported_arrv_li-%element-(component_permission_request-name) = if_abap_behv=>mk-on.
          reported_arrv_li-%msg = NEW zcm_kat1_msg(
                                               severity = if_abap_behv_message=>severity-error
                                               textid    = zcm_kat1_msg=>arrvheader_empty
                                               field     = |{ component_permission_request-name }| ).
          APPEND reported_arrv_li  TO reported-arrival.

        ENDIF.
      ENDLOOP.
    ENDLOOP.
  ENDMETHOD.

*  METHOD checkItemsAssigned.
*
*    READ ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
*    ENTITY ArrvItems
*      FIELDS ( ItemID )
*      WITH CORRESPONDING #( keys )
*      RESULT DATA(lt_items)
*      FAILED DATA(read_failed).
*
*  ENDMETHOD.

  METHOD putInStorage.
  ENDMETHOD.

  METHOD setCreditedStatus.

    MODIFY ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
    ENTITY Arrival
      UPDATE FIELDS ( StatCode PostDate )
      WITH VALUE #( FOR key IN keys
      ( %key = key-%key
        StatCode = arrival_status-credited
        PostDate = cl_abap_context_info=>get_system_date( ) ) )
    FAILED failed
    REPORTED DATA(modify_reported).


    READ ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
    ENTITY Arrival
      ALL FIELDS WITH CORRESPONDING #( keys )
      RESULT DATA(arrivals).

    result = VALUE #( FOR arrival IN arrivals
                        ( %tky   = arrival-%tky
                          %param = arrival ) ).

  ENDMETHOD.

  METHOD cancelCredStatus.

    MODIFY ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
    ENTITY Arrival
      UPDATE FIELDS ( StatCode )
      WITH VALUE #( FOR key IN keys
      ( %key = key-%key
        StatCode = arrival_status-expected ) )
    FAILED failed
    REPORTED DATA(modify_reported).

    READ ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
    ENTITY Arrival
      ALL FIELDS WITH CORRESPONDING #( keys )
      RESULT DATA(arrivals).

    result = VALUE #( FOR arrival IN arrivals
                        ( %tky   = arrival-%tky
                          %param = arrival ) ).

  ENDMETHOD.

  METHOD setReturnsStatus.

    MODIFY ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
    ENTITY Arrival
      UPDATE FIELDS ( StatCode )
      WITH VALUE #( FOR key IN keys
      ( %key = key-%key
        StatCode = arrival_status-returns
        PostDate = cl_abap_context_info=>get_system_date( ) ) )
    FAILED failed
    REPORTED DATA(modify_reported).

    READ ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
    ENTITY Arrival
      ALL FIELDS WITH CORRESPONDING #( keys )
      RESULT DATA(arrivals).

    result = VALUE #( FOR arrival IN arrivals
                        ( %tky   = arrival-%tky
                          %param = arrival ) ).

  ENDMETHOD.

  METHOD get_features.

    READ ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
    ENTITY Arrival
      FIELDS ( ArrvId StatCode )
      WITH CORRESPONDING #( keys )
      RESULT DATA(lt_arrivals)
      FAILED DATA(read_failed).

    result =
        VALUE #(
          FOR ls_arrival IN lt_arrivals
            LET is_active  = COND #( WHEN ls_arrival-StatCode = arrival_status-credited
                                        OR ls_arrival-StatCode = arrival_status-returns
                                      THEN if_abap_behv=>fc-o-disabled
                                      ELSE if_abap_behv=>fc-o-enabled )

                is_cancel   = COND #( WHEN ls_arrival-StatCode = arrival_status-expected
                                        OR ls_arrival-StatCode = arrival_status-returns
                                      THEN if_abap_behv=>fc-o-disabled
                                      ELSE if_abap_behv=>fc-o-enabled )
             IN ( %tky                     = ls_arrival-%tky
                  %field-arrvId = COND #( WHEN ls_arrival-arrvId IS INITIAL
                                                     THEN if_abap_behv=>fc-f-mandatory
                                                     ELSE if_abap_behv=>fc-f-read_only )
                  %action-creditedStatus   = is_active
                  %action-returnsStatus    = is_active
                  %action-cancelCredStatus = is_cancel   ) ).
  ENDMETHOD.
ENDCLASS.
