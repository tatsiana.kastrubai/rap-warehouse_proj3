@EndUserText.label: 'Arrival projection view'
@AccessControl.authorizationCheck: #NOT_REQUIRED
@Search.searchable: true

@Metadata.allowExtensions: true
define view entity ZKAT1_C_ARRIVAL
  as projection on ZKAT1_I_ARRIVAL
{
  key ArrvUUID,
      WrhUUID,
      @Search.defaultSearchElement: true
      @Search.fuzzinessThreshold: 0.90 
      ArrvID,
      @Consumption.valueHelpDefinition: [{ entity: { name: 'ZKAT1_I_DOCTYPE', element: 'DocCode'} }]
      @ObjectModel.text.element: ['DocCodeDesc']
      @Search.defaultSearchElement: true
      DoctID,
      _Document.DocCodeDesc,
      @Consumption.valueHelpDefinition: [{ entity: { name: 'ZKAT1_I_COMPANY', element: 'Payer'} }]
      @ObjectModel.text.element: ['Name']
      @Search.defaultSearchElement: true
      SuplId  as Company,
      _Company.Name,
      @Search.defaultSearchElement: true
      ArrvDate,
      PostDate,
      @Consumption.valueHelpDefinition: [{ entity: { name: 'ZKAT1_I_STATUS', element: 'StatCode'} }]
      @ObjectModel.text.element: ['StatCodeDesc']
      @Search.defaultSearchElement: true
      StatCode,
      _Status.StatCodeDesc,
      StatusCriticality,
      CreateBy,
      CreateAt,
      LastChangedBy,
      LastChangedAt,

      _Wrhous : redirected to parent ZKAT1_C_WRHOUS,
      _ArrvItems : redirected to composition child ZKAT1_C_ARRIVALITEMS,
      _Status,
      _Company,
      _Document
}
