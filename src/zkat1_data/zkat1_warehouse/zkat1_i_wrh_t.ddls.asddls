@EndUserText.label: 'Wrh description int view'
@AccessControl.authorizationCheck: #NOT_REQUIRED
define view entity ZKAT1_I_WRH_T
  as select from zkat1_d_wrh_t
{
  key wrh_id     as WrhID,
      short_name as WrhName,
      note       as Note
}
where
  spras = $session.system_language
