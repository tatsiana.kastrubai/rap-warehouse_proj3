@EndUserText.label: 'Composite CDS view: storage'
@AccessControl.authorizationCheck: #NOT_REQUIRED
@Search.searchable: true

@Metadata.allowExtensions: true
define view entity ZKAT1_C_STORAGE 
 as projection on ZKAT1_I_STOR 
{
    key StorUUID,
    WrhUUID,
    StorID,
    @Search.defaultSearchElement: true
    MatrID,
    
    _Wrhous : redirected to parent ZKAT1_C_WRHOUS
}
