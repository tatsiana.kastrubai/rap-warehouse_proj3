CLASS lhc_wrhous DEFINITION INHERITING FROM cl_abap_behavior_handler.
  PRIVATE SECTION.
    METHODS calculateWrhId FOR DETERMINE ON SAVE
      IMPORTING keys FOR  Wrhous~calculateWrhID.

    METHODS createWrhStorage FOR DETERMINE ON SAVE
      IMPORTING keys FOR Wrhous~createWrhStorage.

    METHODS checkArrivalsValid FOR DETERMINE ON MODIFY
      IMPORTING keys FOR Wrhous~checkArrivalsValid.

    METHODS validateWrhType FOR VALIDATE ON SAVE
      IMPORTING keys FOR Wrhous~validateWrhType.

    METHODS validateLocation FOR VALIDATE ON SAVE
      IMPORTING keys FOR Wrhous~validateLocation.

    METHODS validateEmployee FOR VALIDATE ON SAVE
      IMPORTING keys FOR Wrhous~validateEmployee.

*    METHODS get_instance_features FOR INSTANCE FEATURES
*      IMPORTING keys REQUEST requested_features FOR Wrhous RESULT result.
ENDCLASS.

CLASS lhc_wrhous IMPLEMENTATION.
  METHOD calculateWrhId.

    " check if ID is already filled
    READ ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
      ENTITY Wrhous
        FIELDS ( WrhID ) WITH CORRESPONDING #( keys )
      RESULT DATA(wrh_data).

    " remove lines where ID is already filled.
    DELETE wrh_data WHERE WrhID IS NOT INITIAL.

    " anything left ?
    CHECK wrh_data IS NOT INITIAL.

    " Select max wrhous ID
    SELECT SINGLE
        FROM  zkat1_d_wrh_a
        FIELDS MAX( wrh_id ) AS wrhlID
        INTO @DATA(lv_max_wrhid).

    IF lv_max_wrhid IS INITIAL OR lv_max_wrhid < 70000.
      lv_max_wrhid = 70000.  " Starting number
    ENDIF.

    " Set the wrhous ID
    MODIFY ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
    ENTITY Wrhous
      UPDATE
        FROM VALUE #( FOR wrhous IN wrh_data INDEX INTO i (
          %tky              = wrhous-%tky
          WrhID             = lv_max_wrhid + 1
          %control-WrhID = if_abap_behv=>mk-on ) )
    REPORTED DATA(update_reported).

    reported = CORRESPONDING #( DEEP update_reported ).
  ENDMETHOD.

  METHOD createWrhStorage.

    READ ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
      ENTITY Wrhous
          FIELDS ( WrhUUID WrhID ) WITH CORRESPONDING #( keys )
      RESULT DATA(wrh_data).

    READ TABLE wrh_data INTO DATA(ls_wrh) INDEX 1.

    DATA ls_wrhtype TYPE zkat1_d_wrhtyp_t.
    SELECT SINGLE * FROM zkat1_d_wrhtyp_t
       WHERE wrhtype = @ls_wrh-WrhType
*         AND spras = @cl_abap_context_info=>get_user_language_iso_format.
       INTO CORRESPONDING FIELDS OF @ls_wrhtype.

    READ ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
      ENTITY Storage
         ALL FIELDS WITH CORRESPONDING #( keys )
      RESULT DATA(lt_stordata).


    IF lt_stordata IS INITIAL.
*      DATA object_id TYPE char3.
*      object_id = 0.
      DO ls_wrhtype-max_size TIMES.
*        object_id = object_id + 1.
*        MODIFY ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
*          ENTITY Storage
*            CREATE
*            SET FIELDS WITH VALUE #( ( %cid = 'MyContentID_1'
*                                       WrhUUID = ls_wrh-WrhUUID
*                                       StorId = | { ls_wrhtype-stor_pref }{ object_id ALPHA = IN } |
*                                       IsEmpty = abap_true
*                                       LastChangedBy = cl_abap_context_info=>get_user_alias( )
*                                       LastChangedAt = cl_abap_context_info=>get_system_date( ) ) )
*            MAPPED DATA(mapped)
*            FAILED DATA(failed)
*            REPORTED DATA(create_reported).

*        COMMIT ENTITIES
*          RESPONSE OF zi_travel_ve_m_xxx
*          FAILED     DATA(failed_commit)
*          REPORTED   DATA(reported_commit).

      ENDDO.

    ENDIF.


  ENDMETHOD.

  METHOD checkArrivalsValid.

    DATA permission_request TYPE STRUCTURE FOR PERMISSIONS REQUEST zkat1_i_arrival.
    DATA reported_arrv_li LIKE LINE OF reported-arrival.

    DATA(description_permission_request) = CAST cl_abap_structdescr( cl_abap_typedescr=>describe_by_data_ref( REF #( permission_request-%field ) ) ).
    DATA(components_permission_request) = description_permission_request->get_components(  ).

    LOOP AT components_permission_request INTO DATA(component_permission_request).
      permission_request-%field-(component_permission_request-name) = if_abap_behv=>mk-on.
    ENDLOOP.

    " Get current field values
    READ ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
    ENTITY Arrival
      ALL FIELDS
      WITH CORRESPONDING #( keys )
      RESULT DATA(lt_arrivals).

    LOOP AT lt_arrivals INTO DATA(ls_arrv).

      GET PERMISSIONS ONLY INSTANCE FEATURES ENTITY zkat1_i_arrival
                FROM VALUE #( ( ArrvUUID = ls_arrv-ArrvUUID ) )
                REQUEST permission_request
                RESULT DATA(permission_result)
                FAILED DATA(failed_permission_result)
                REPORTED DATA(reported_permission_result).

      LOOP AT components_permission_request INTO component_permission_request.

        IF permission_result-global-%field-(component_permission_request-name) = if_abap_behv=>fc-f-mandatory AND
           ls_arrv-(component_permission_request-name) IS INITIAL.

*          APPEND VALUE #( %tky = ls_arrv-%tky ) TO failed-.

          CLEAR reported_arrv_li.
          reported_arrv_li-%tky = ls_arrv-%tky.
          reported_arrv_li-%element-(component_permission_request-name) = if_abap_behv=>mk-on.
          reported_arrv_li-%msg = NEW zcm_kat1_msg(
                                               severity = if_abap_behv_message=>severity-error
                                               textid    = zcm_kat1_msg=>arrvheader_empty
                                               field     = |{ component_permission_request-name }| ).
          APPEND reported_arrv_li  TO reported-arrival.

        ENDIF.

      ENDLOOP.

    ENDLOOP.

  ENDMETHOD.

  METHOD validateWrhType.

    " Read relevant Wrhous instance data
    READ ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
      ENTITY Wrhous
        FIELDS ( WrhType ) WITH CORRESPONDING #( keys )
      RESULT DATA(wrh_data).

*    DATA wtypes TYPE SORTED TABLE OF *** WITH UNIQUE KEY wrhtype.

    LOOP AT wrh_data INTO DATA(wtype).
      IF wtype-WrhType IS INITIAL OR wtype-WrhType < 1
                                  OR wtype-WrhType > 4.
        APPEND VALUE #(  %tky = wtype-%tky ) TO failed-wrhous.

        APPEND VALUE #(  %tky        = wtype-%tky
                         %state_area = 'VALIDATE_WRHOUS'
                         %msg        = NEW zcm_kat1_msg(
                                           severity  = if_abap_behv_message=>severity-error
                                           textid    = zcm_kat1_msg=>wrhtype_unknown )
                         %element-WrhType = if_abap_behv=>mk-on )
          TO reported-wrhous.
      ENDIF.
    ENDLOOP.

  ENDMETHOD.

  METHOD validateLocation.

    READ ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
      ENTITY Wrhous
        FIELDS ( AdrsID ) WITH CORRESPONDING #( keys )
      RESULT DATA(wrh_data).

    DATA wlocts TYPE SORTED TABLE OF zkat1_d_loctn_a WITH UNIQUE KEY loc_id.

    wLocts = CORRESPONDING #( wrh_data DISCARDING DUPLICATES MAPPING loc_id = AdrsID EXCEPT * ).
    DELETE wlocts WHERE loc_id IS INITIAL.

    IF wlocts IS NOT INITIAL.
      SELECT FROM zkat1_d_loctn_a FIELDS loc_id
        FOR ALL ENTRIES IN @wLocts
        WHERE loc_id = @wlocts-loc_id
        INTO TABLE @DATA(gt_loc).
    ENDIF.

    LOOP AT wrh_data INTO DATA(wloc).
      IF wloc-AdrsID IS INITIAL OR NOT line_exists( gt_loc[ loc_id = wloc-AdrsID ] ).
        APPEND VALUE #(  %tky = wloc-%tky ) TO failed-wrhous.

        APPEND VALUE #(  %tky        = wloc-%tky
                         %state_area = 'VALIDATE_LOCATION'
                         %msg        = NEW zcm_kat1_msg(
                                           severity   = if_abap_behv_message=>severity-error
                                           textid     = zcm_kat1_msg=>location_unknown
                                           location   = wloc-AdrsID )
                         %element-WrhType = if_abap_behv=>mk-on )
          TO reported-wrhous.
      ENDIF.
    ENDLOOP.

  ENDMETHOD.

  METHOD validateEmployee.

    READ ENTITIES OF zkat1_i_wrhous IN LOCAL MODE
      ENTITY Wrhous
        FIELDS ( EmplID ) WITH CORRESPONDING #( keys )
      RESULT DATA(wrh_data).

    DATA wEmpls TYPE SORTED TABLE OF zkat1_d_employ_a WITH UNIQUE KEY empl_id.

    wEmpls = CORRESPONDING #( wrh_data DISCARDING DUPLICATES MAPPING empl_id = EmplID EXCEPT * ).
    DELETE wEmpls WHERE empl_id IS INITIAL.

    IF wEmpls IS NOT INITIAL.
      SELECT FROM zkat1_d_employ_a FIELDS empl_id
        FOR ALL ENTRIES IN @wEmpls
        WHERE empl_id = @wEmpls-empl_id
        INTO TABLE @DATA(gt_empl).
    ENDIF.

    LOOP AT wrh_data INTO DATA(wempl).
      IF wempl-EmplID IS INITIAL OR NOT line_exists( gt_empl[ empl_id = wempl-EmplID ] ).
        APPEND VALUE #(  %tky = wempl-%tky ) TO failed-wrhous.

        APPEND VALUE #(  %tky        = wempl-%tky
                         %state_area = 'VALIDATE_EMPLOYEE'
                         %msg        = NEW zcm_kat1_msg(
                                           severity   = if_abap_behv_message=>severity-error
                                           textid     = zcm_kat1_msg=>employee_unknown
                                           employee   = wempl-EmplID )
                         %element-WrhType = if_abap_behv=>mk-on )
          TO reported-wrhous.
      ENDIF.
    ENDLOOP.

  ENDMETHOD.

*  METHOD get_instance_features.
*  ENDMETHOD.

ENDCLASS.
