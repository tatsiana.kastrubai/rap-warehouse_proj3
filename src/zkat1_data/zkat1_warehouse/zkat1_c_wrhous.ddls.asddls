@EndUserText.label: 'Warehouse projection view'
@AccessControl.authorizationCheck: #NOT_REQUIRED
@Search.searchable: true

@Metadata.allowExtensions: true
define root view entity ZKAT1_C_WRHOUS
  provider contract transactional_query
  as projection on ZKAT1_I_WRHOUS 
{
  key WrhUUID,
      WrhID,
      @Consumption.valueHelpDefinition: [{ entity: { name: 'ZKAT1_I_WRHTYPE2', element: 'WrhType'} }]
      @ObjectModel.text.element: ['TypeDescr']
      @Search.defaultSearchElement: true
      WrhType,
      _WrhType.TypeDescr,
      _Places.FreePlaces as FreePlaces,
      Criticality,
      _ArrivalCount.PendingProc as PendingProc,
      @Consumption.valueHelpDefinition: [{ entity: { name: 'ZKAT1_I_ONELINE_LOC', element: 'AdrsID'} }]
      @ObjectModel.text.element: ['Adress']
      @Search.defaultSearchElement: true
      AdrsID,
      _Location.Adress,
      @Consumption.valueHelpDefinition: [{ entity: { name: 'ZKAT1_I_EMPLOYEE', element: 'EmplID'} }]
      @ObjectModel.text.element: ['Employee']
      @Search.defaultSearchElement: true
      EmplID,
      _Employee.EmployeeName as Employee,
      _Employee.JobTitle,
      _Employee.Phone,
      
      LastChangedBy,
      LastChangedAt,
//      ImageUrl,

      _Arrival : redirected to composition child ZKAT1_C_ARRIVAL,
      _Storage : redirected to composition child ZKAT1_C_STORAGE,
      _Places,
      _Location,
      _Employee,
      _WrhType,
      _ArrivalCount
    
}
