@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Warehouse int view'
define root view entity ZKAT1_I_WRHOUS
  as select from zkat1_d_wrh_a as Wrhous
  composition [0..*] of ZKAT1_I_ARRIVAL    as _Arrival
  composition [0..*] of ZKAT1_I_STOR       as _Storage
  
  association to ZKAT1_I_TOTALPL           as _Places       on  $projection.WrhUUID = _Places.WrhUUID
  association to ZKAT1_I_ARRV_COUNT_BY_WRH as _ArrivalCount on  $projection.WrhUUID    = _ArrivalCount.WrhUUID
                                                            and _ArrivalCount.StatCode = '1'
  association to ZKAT1_I_ONELINE_LOC       as _Location     on  $projection.AdrsID = _Location.AdrsID
  association to ZKAT1_I_EMPLOYEE          as _Employee     on  $projection.EmplID = _Employee.EmplID
  association to ZKAT1_I_WRHTYPE2          as _WrhType      on  $projection.WrhType = _WrhType.WrhType
  //  association to ZKAT1_I_WRHTYPE        as _WrhType  on  $projection.WrhType  = _WrhType.value_low
  //                                                     and _WrhType.domain_name = 'ZKAT1_WRHTYPE'
  //                                                     and _WrhType.language    = $session.system_language

{
  key wrh_uuid        as WrhUUID,
      wrh_id          as WrhID,
      wrhtype         as WrhType,
      loc_id          as AdrsID,
      empl_id         as EmplID,
      _ArrivalCount.PendingProc,
      case _ArrivalCount.PendingProc
        when 0 then 0
        else 2 end as Criticality,
      last_changed_by as LastChangedBy,
      last_changed_at as LastChangedAt,
//      'sap-icon://capital-projects' as ImageUrl,

      _Arrival,
      _Storage,
      _Places,
      _WrhType,
      _Location,
      _Employee,
      _ArrivalCount

}
