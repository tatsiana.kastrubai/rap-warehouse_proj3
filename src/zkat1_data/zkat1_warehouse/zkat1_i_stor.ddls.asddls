@EndUserText.label: 'Storage int view'
@AccessControl.authorizationCheck: #NOT_REQUIRED
define view entity ZKAT1_I_STOR
  as select from zkat1_d_stor_a as Storage
  association to parent ZKAT1_I_WRHOUS       as _Wrhous   on  $projection.WrhUUID = _Wrhous.WrhUUID
{
  key stor_uuid       as StorUUID,
      wrh_uuid        as WrhUUID,
      stor_id         as StorID,
      material        as MatrID,
      is_empty        as IsEmpty,
      last_changed_by as LastChangedBy,
      last_changed_at as LastChangedAt,
      
      _Wrhous 
}

