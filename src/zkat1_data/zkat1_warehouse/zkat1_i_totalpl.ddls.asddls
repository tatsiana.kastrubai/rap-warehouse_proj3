@EndUserText.label: 'Storage int view'
@AccessControl.authorizationCheck: #NOT_REQUIRED
define view entity ZKAT1_I_TOTALPL
  as select from zkat1_d_stor_a as Storage
{
  key wrh_uuid  as WrhUUID,
      count( distinct stor_uuid ) as Total,
      sum( is_empty )  as Free,
      division( sum( is_empty ) , count( distinct stor_uuid ) , 2 ) * 100 as FreePlaces
}
group by
    wrh_uuid

