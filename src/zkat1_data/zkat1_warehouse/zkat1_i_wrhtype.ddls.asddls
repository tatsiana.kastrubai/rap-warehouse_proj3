@EndUserText.label: 'Warehouse Type int view'
@AccessControl.authorizationCheck: #NOT_REQUIRED
@Search.searchable: true
@ObjectModel.resultSet.sizeCategory: #XS
define view entity ZKAT1_I_WRHTYPE
  as select from DDCDS_CUSTOMER_DOMAIN_VALUE_T
                 ( p_domain_name: 'ZKAT_WRHTYPE')
{
      @UI.hidden: true
  key domain_name,
      @UI.hidden: true
  key language,
      @UI.hidden: true
  key value_position,
      @Search.defaultSearchElement: true
      @Search.fuzzinessThreshold: 0.8
      @Semantics.organization.name: true
      value_low,
      @Semantics.text: true
      text
}
