CLASS zcl_gen_wrh_data_kat1 DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    INTERFACES if_oo_adt_classrun.
  PROTECTED SECTION.
  PRIVATE SECTION.
    METHODS fill_wrh.
ENDCLASS.



CLASS ZCL_GEN_WRH_DATA_KAT1 IMPLEMENTATION.


  METHOD if_oo_adt_classrun~main.
    me->fill_wrh(  ).
    out->write( 'data was inserted successfully!' ).
  ENDMETHOD.


  METHOD fill_wrh.
    DATA it_wrh TYPE TABLE OF zkat1_d_wrh_a.
    DATA it_wrh_t TYPE TABLE OF zkat1_d_wrh_t.
    DATA it_stor TYPE TABLE OF zkat1_d_stor_a.
    DATA it_wrhtyp TYPE TABLE OF zkat1_d_wrhtyp_t.

    DATA it_loctn TYPE TABLE OF zkat1_d_loctn_a.

    it_wrh = VALUE #(
          ( wrh_id = '70001' empl_id = '400001' loc_id = '200001' wrhtype = '1')
          ( wrh_id = '70002' empl_id = '400002' loc_id = '200001' wrhtype = '4')
          ( wrh_id = '70003' empl_id = '400003' loc_id = '200002' wrhtype = '2')
          ( wrh_id = '70004' empl_id = '400004' loc_id = '200001' wrhtype = '1')
          ( wrh_id = '70005' empl_id = '400002' loc_id = '200001' wrhtype = '3')
          ( wrh_id = '70006' empl_id = '400002' loc_id = '200002' wrhtype = '4')
          ( wrh_id = '70007' empl_id = '400003' loc_id = '200003' wrhtype = '4') ).

    it_wrh_t = VALUE #(
          ( spras  = 'E' wrh_id = '70001' short_name = 'Warehouse #1' note = '' )
          ( spras  = 'E' wrh_id = '70002' short_name = 'Warehouse #2' note = '' )
          ( spras  = 'E' wrh_id = '70003' short_name = 'Warehouse #3' note = '' )
          ( spras  = 'E' wrh_id = '70004' short_name = 'Warehouse #4' note = '' )
          ( spras  = 'E' wrh_id = '70005' short_name = 'Warehouse #5' note = '' )
          ( spras  = 'E' wrh_id = '70006' short_name = 'Warehouse #6' note = '' )
          ( spras  = 'E' wrh_id = '70007' short_name = 'Warehouse #7' note = '' ) ).

    it_stor = VALUE #(
              ( wrh_uuid = 'A6CE48C494271EEDA39E7F73FDE31C68' stor_id = 'S7-001' material = ''  is_empty = 1 )
              ( wrh_uuid = 'A6CE48C494271EEDA39E7F73FDE31C68' stor_id = 'S7-002' material = ''  is_empty = 1 )
              ( wrh_uuid = 'A6CE48C494271EEDA39E7F73FDE31C68' stor_id = 'S7-003' material = ''  is_empty = 1 )
              ( wrh_uuid = 'A6CE48C494271EEDA39E7F73FDE31C68' stor_id = 'S7-004' material = '17001' )
              ( wrh_uuid = 'A6CE48C494271EEDA39E7F73FDE31C68' stor_id = 'S7-005' material = '17002' )
              ( wrh_uuid = 'A6CE48C494271EEDA39E7F73FDE31C68' stor_id = 'S7-006' material = ''  is_empty = 1 )
              ( wrh_uuid = 'A6CE48C494271EEDA39E7F73FDE31C68' stor_id = 'S7-007' material = ''  is_empty = 1 )
              ( wrh_uuid = 'A6CE48C494271EEDA39E7F73FDE31C68' stor_id = 'S7-008' material = ''  is_empty = 1 )
              ( wrh_uuid = 'A6CE48C494271EEDA39E7F73FDE31C68' stor_id = 'S7-009' material = ''  is_empty = 1 )
              ( wrh_uuid = 'A6CE48C494271EEDA39E7F73FDE31C68' stor_id = 'S7-010' material = ''  is_empty = 1 )
              ( wrh_uuid = '1EACE85DF05C1EEDA39E341A50E65E51' stor_id = 'S1-000' material = ''  is_empty = 1 )
              ( wrh_uuid = '1EACE85DF05C1EEDA39E341A50E65E51' stor_id = 'S2-009' material = ''  is_empty = 1 )
              ( wrh_uuid = '1EACE85DF05C1EEDA39E341A50E65E51' stor_id = 'S2-008' material = '17011' )
              ( wrh_uuid = '1EACE85DF05C1EEDA39E341A50E65E51' stor_id = 'S2-007' material = ''  is_empty = 1 )
              ( wrh_uuid = '1EACE85DF05C1EEDA39E341A50E65E51' stor_id = 'S2-006' material = ''  is_empty = 1 )
              ( wrh_uuid = '1EACE85DF05C1EEDA39E341A50E65E51' stor_id = 'S2-005' material = '17013' )
              ( wrh_uuid = '1EACE85DF05C1EEDA39E341A50E65E51' stor_id = 'S1-001' material = ''  is_empty = 1 )
              ( wrh_uuid = '1EACE85DF05C1EEDA39E341A50E5DE51' stor_id = 'S1-002' material = ''  is_empty = 1 )
              ( wrh_uuid = '1EACE85DF05C1EEDA39E341A50E5DE51' stor_id = 'S1-003' material = '17017' )
              ( wrh_uuid = '1EACE85DF05C1EEDA39E341A50E5DE51' stor_id = 'S1-004' material = ''  is_empty = 1 )
              ( wrh_uuid = '1EACE85DF05C1EEDA39E341A50E5DE51' stor_id = 'S1-005' material = '17039' )
              ( wrh_uuid = '1EACE85DF05C1EEDA39E341A50E5DE51' stor_id = 'S1-006' material = '17039' )
              ( wrh_uuid = '1EACE85DF05C1EEDA39E341A50E5DE51' stor_id = 'S1-007' material = ''  is_empty = 1 )
              ( wrh_uuid = '1EACE85DF05C1EEDA39E341A50E5DE51' stor_id = 'S1-008' material = ''  is_empty = 1 )
              ( wrh_uuid = '1EACE85DF05C1EEDA39E341A50E5DE51' stor_id = 'S1-009' material = ''  is_empty = 1 )
              ( wrh_uuid = '1EACE85DF05C1EEDA39E341A50E5DE51' stor_id = 'S1-000' material = ''  is_empty = 1 ) ).


    LOOP AT it_stor ASSIGNING FIELD-SYMBOL(<fs_stor>).
      <fs_stor>-stor_uuid = cl_system_uuid=>if_system_uuid_static~create_uuid_x16(  ).
    ENDLOOP.

*    DELETE FROM zkat1_d_wrh_a.
*    INSERT zkat1_d_wrh_a FROM TABLE @it_wrh.
*
*    DELETE FROM zkat1_d_wrh_t.
*    INSERT zkat1_d_wrh_t FROM TABLE @it_wrh_t.

    DELETE FROM zkat1_d_stor_a.
    INSERT zkat1_d_stor_a FROM TABLE @it_stor.

    it_wrhtyp = VALUE #(
                ( spras  = 'E' wrhtype = '1' type_text = 'Block' stor_pref = 'STB-' max_size = 10 )
                ( spras  = 'E' wrhtype = '2' type_text = 'Mixed' stor_pref = 'STM-' max_size = 30 )
                ( spras  = 'E' wrhtype = '3' type_text = 'Rack' stor_pref = 'STR-' max_size = 20 )
                ( spras  = 'E' wrhtype = '4' type_text = 'Pallet' stor_pref = 'STP-' max_size = 40 )
                ( spras  = 'R' wrhtype = '1' type_text = 'Блочный' stor_pref = 'STB-' max_size = 10 )
                ( spras  = 'R' wrhtype = '2' type_text = 'Смешанный' stor_pref = 'STM-' max_size = 30 )
                ( spras  = 'R' wrhtype = '4' type_text = 'Паллеты' stor_pref = 'STP-' max_size = 40 ) ).

    DELETE FROM zkat1_d_wrhtyp_t.
    INSERT zkat1_d_wrhtyp_t FROM TABLE @it_wrhtyp.

  ENDMETHOD.
ENDCLASS.
