@EndUserText.label: 'Company int view'
@AccessControl.authorizationCheck: #NOT_REQUIRED
define view entity ZKAT1_I_COMPANY
  as select from zkat1_d_suppli_t
{
  key spras        as Spras,
  key payer        as Payer,
      company_name as Name
}
where
  spras = $session.system_language
