CLASS zcl_kat1_gen_data_supplier DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    INTERFACES if_oo_adt_classrun.
  PROTECTED SECTION.
  PRIVATE SECTION.
    METHODS fill_supplier.
ENDCLASS.



CLASS ZCL_KAT1_GEN_DATA_SUPPLIER IMPLEMENTATION.


   METHOD fill_supplier.
    DATA: lt_company TYPE TABLE OF zkat1_d_suppli_t,
          lt_supplier TYPE TABLE OF zkat1_d_suppli_a,
          lv_created_at TYPE timestampl.

    lt_company = VALUE #( ( spras ='E' payer ='234990001' company_name ='LLC Aerobel'  )
         ( spras ='E' payer ='960023373' company_name ='LLC ModniPrint' )
         ( spras ='E' payer ='470567701' company_name ='JSC Belalyans'  )
         ( spras ='R' payer ='960023373' company_name ='ООО Модный принт' )
         ( spras ='R' payer ='234990001' company_name ='ООО Аэробел'  )
         ( spras ='R' payer ='470567701' company_name ='ОАО Белальянс') ).

   lt_supplier = VALUE #( ( payer ='234990001' ladr_id ='000001' phone ='394563928' )
         ( payer ='470567701' ladr_id ='000002' phone ='333070271' ) ).

    DELETE FROM zkat1_d_suppli_t.
    INSERT zkat1_d_suppli_t FROM TABLE @lt_company.

   LOOP AT lt_supplier ASSIGNING FIELD-SYMBOL(<fs_supplier>).
          GET TIME STAMP FIELD lv_created_at.
          <fs_supplier>-supl_uuid = cl_system_uuid=>if_system_uuid_static~create_uuid_x16(  ).
          <fs_supplier>-supl_changed_at = lv_created_at.
          WAIT UP TO 1 SECONDS.
    ENDLOOP.

*     DELETE FROM zkat1_d_suppli_a.
*    INSERT zkat1_d_suppli_a FROM TABLE @lt_supplier.

    ENDMETHOD.


  METHOD if_oo_adt_classrun~main.
    me->fill_supplier(  ).
  ENDMETHOD.
ENDCLASS.
