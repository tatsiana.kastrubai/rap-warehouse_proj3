@EndUserText.label: 'Supplier projection view'
@AccessControl.authorizationCheck: #NOT_REQUIRED
@Search.searchable: true
@Metadata.allowExtensions: true
define root view entity ZKAT1_C_SUPPLIER
  provider contract transactional_query
  as projection on ZKAT1_I_SUPPLIER
{
      @Search.defaultSearchElement: true
  key SuplUUID,
  key Payer,
      @Search: { defaultSearchElement: true }
      _Company.Name as Company,
      AdrsId,
      _Location.Adress as Adress,
      Phone,
      SuplChangedAt,
      /* Associations */
      _Company,
      _Location

}
