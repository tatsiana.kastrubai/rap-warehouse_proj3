@EndUserText.label: 'Supplier int view'
@AccessControl.authorizationCheck: #NOT_REQUIRED
define root view entity ZKAT1_I_SUPPLIER
  as select from zkat1_d_suppli_a
  /*+[hideWarning] { "IDS" : [ "CARDINALITY_CHECK" ] } */
  association to ZKAT1_I_COMPANY     as _Company  on  _Company.Payer = $projection.Payer
  /*+[hideWarning] { "IDS" : [ "CARDINALITY_CHECK" ] } */
  association to ZKAT1_I_ONELINE_LOC as _Location on  _Location.AdrsID = $projection.AdrsId
{
  key supl_uuid       as SuplUUID,
  key payer           as Payer,
      ladr_id         as AdrsId,
      phone           as Phone,

      @EndUserText.label: 'Last Changed At'
      @Semantics.systemDateTime.lastChangedAt: true
      supl_changed_at as SuplChangedAt,
      
      _Company,
      _Location
}
