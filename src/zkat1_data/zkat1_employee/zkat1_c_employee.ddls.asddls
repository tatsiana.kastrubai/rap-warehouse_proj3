@EndUserText.label: 'Employee projection view'
@AccessControl.authorizationCheck: #NOT_REQUIRED
@Search.searchable: true
@Metadata.allowExtensions: true

define root view entity ZKAT1_C_EMPLOYEE
  provider contract transactional_query
  as projection on ZKAT1_I_EMPLOYEE as Empl
{
  key EmplID,
      EmployeeName,
      @Search: { defaultSearchElement: true }
      PosCode,
      JobTitle,
      Phone,
//      EmplChangedAt,

      /* Associations */
      _Person,
      _Positn
}
