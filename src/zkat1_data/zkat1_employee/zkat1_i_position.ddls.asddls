@EndUserText.label: 'Empoyee position int view'
@AccessControl.authorizationCheck: #NOT_REQUIRED
@ObjectModel.dataCategory:#TEXT
define view entity ZKAT1_I_POSITION
  as select from zkat1_d_positn_t
{
//  @Semantics.language: true
//  key spras       as Spras,
  key pos_id      as PosCode,
      @Semantics.text: true
      description as JobTitle
}
where
    spras = $session.system_language
