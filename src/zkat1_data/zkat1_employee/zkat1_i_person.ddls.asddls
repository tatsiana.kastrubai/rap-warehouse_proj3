@EndUserText.label: 'Person int view'
@AccessControl.authorizationCheck: #NOT_REQUIRED
@ObjectModel.dataCategory:#TEXT
define root view entity ZKAT1_I_PERSON
  as select from zkat1_d_person_t
{
//  @Semantics.language: true
//  key spras           as Spras,
  key pers_id         as PersId,
      @Semantics.text: true
      first_name      as Name,
      @Semantics.text: true
      last_name       as Surname
}
where
    spras = $session.system_language
