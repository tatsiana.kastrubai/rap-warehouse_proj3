@EndUserText.label: 'Employee int view'
@AccessControl.authorizationCheck: #NOT_REQUIRED
@Search.searchable: true

@Metadata.allowExtensions: true
define root view entity ZKAT1_I_EMPLOYEE
  as select from zkat1_d_employ_a
  association to ZKAT1_I_PERSON   as _Person on  _Person.PersId = $projection.EmplID
  association to ZKAT1_I_POSITION as _Positn on  _Positn.PosCode = $projection.PosCode
{
      @Search.defaultSearchElement: true
      @Search.fuzzinessThreshold: 0.8
      @Semantics.text: true
  key empl_id                                             as EmplID,
      concat_with_space(_Person.Name, _Person.Surname, 1) as EmployeeName,
      @Search.defaultSearchElement: true
      @Search.fuzzinessThreshold: 0.8
      @Semantics.text: true
      pos_id                                              as PosCode,
      _Positn.JobTitle                                    as JobTitle,
      phone                                               as Phone,
//      @EndUserText.label: 'Last Changed At'
//      @Semantics.systemDateTime.lastChangedAt: true
//      empl_changed_at                                     as EmplChangedAt,

      _Person,
      _Positn
}
