CLASS zcl_kat1_gen_data_employee DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    INTERFACES if_oo_adt_classrun.
  PROTECTED SECTION.
  PRIVATE SECTION.
    METHODS fill_employee.
ENDCLASS.



CLASS ZCL_KAT1_GEN_DATA_EMPLOYEE IMPLEMENTATION.


  METHOD if_oo_adt_classrun~main.
    me->fill_employee(  ).
  ENDMETHOD.


   METHOD fill_employee.
    DATA: lt_employee TYPE TABLE OF zkat1_d_employ_a,
          lt_position TYPE TABLE OF zkat1_d_positn_t,
          lt_person   TYPE TABLE OF zkat1_d_person_t,
          lv_created_at TYPE timestampl.

    lt_position = VALUE #( ( spras ='E' pos_id ='000001' description ='Manager'  )
         ( spras ='E' pos_id ='000002' description ='Director'  )
         ( spras ='E' pos_id ='000003' description ='Storekeeper'  )
         ( spras ='R' pos_id ='000001' description ='Менеджер'  )
         ( spras ='R' pos_id ='000002' description ='Директор'  )
         ( spras ='R' pos_id ='000003' description ='Кладовщик') ).

   lt_person = VALUE #( ( spras ='E' pers_id ='400001' first_name ='Lolita' last_name ='Popovich' )
         ( spras ='E' pers_id ='400002' first_name ='Ann' last_name ='Rudenko' )
         ( spras ='E' pers_id ='400003' first_name ='Ilaiy' last_name ='Vitovt' )
         ( spras ='E' pers_id ='400004' first_name ='Filip' last_name ='Andjei' )
         ( spras ='R' pers_id ='400001' first_name ='Лолита' last_name ='Попович' )
         ( spras ='R' pers_id ='400002' first_name ='Анна' last_name ='Руденко' )
         ( spras ='R' pers_id ='400003' first_name ='Илья' last_name ='Витовт' )
         ( spras ='R' pers_id ='400004' first_name ='Филипп' last_name ='Андрей' )
         ( spras ='E' pers_id ='400005' first_name ='Greta' last_name ='Yо' ) ).

    DELETE FROM zkat1_d_positn_t.
    INSERT zkat1_d_positn_t FROM TABLE @lt_position.

    DELETE FROM zkat1_d_person_t.
    INSERT zkat1_d_person_t FROM TABLE @lt_person.

    lt_employee = Value #( ( empl_id = '400001' pos_id = '000003' phone = '' )
                           ( empl_id = '400002' pos_id = '000002' phone = '+375293077777' )
                           ( empl_id = '400003' pos_id = '000001' phone = '+480443070651' )
                           ( empl_id = '400004' pos_id = '000003' phone = '+375293070271' )
                           ( empl_id = '400005' pos_id = '000001' phone = '+080493070000' ) ).

    LOOP AT lt_employee ASSIGNING FIELD-SYMBOL(<fs_employee>).
          GET TIME STAMP FIELD lv_created_at.
*          <fs_employee>-empl_uuid = cl_system_uuid=>if_system_uuid_static~create_uuid_x16(  ).
          <fs_employee>-empl_changed_at = lv_created_at.
*          WAIT UP TO 1 SECONDS.
    ENDLOOP.

    DELETE FROM zkat1_d_employ_a.
    INSERT zkat1_d_employ_a FROM TABLE @lt_employee.

    ENDMETHOD.
ENDCLASS.
