@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Address one line int view'
@Search.searchable: true
define view entity ZKAT1_I_ONELINE_LOC
  as select from zkat1_d_loctn_t
{
  key loc_id              as AdrsID,
      @Search.defaultSearchElement: true
      @Search.fuzzinessThreshold: 0.8
      @Semantics.text: true
      country             as County,
      @Search.defaultSearchElement: true
      @Search.fuzzinessThreshold: 0.8
      @Semantics.text: true
      concat_with_space( concat(postcode,','),
                         concat_with_space( 'st.',
                                           concat_with_space( concat(street,','),
                                                              concat_with_space( concat(build,','),
                                                                                 city, 1 ),
                                                              1),
                                           1),
                         1) as Adress
//      @EndUserText.label: 'Last Changed At'
//      @Semantics.systemDateTime.lastChangedAt: true
//      last_changed_at     as LastChangedAt
}
where
  spras = $session.system_language
