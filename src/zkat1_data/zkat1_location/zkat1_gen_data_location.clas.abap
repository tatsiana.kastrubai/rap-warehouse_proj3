CLASS zkat1_gen_data_location DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    INTERFACES if_oo_adt_classrun.
  PROTECTED SECTION.
  PRIVATE SECTION.
    METHODS fill_loctn.
ENDCLASS.



CLASS ZKAT1_GEN_DATA_LOCATION IMPLEMENTATION.


  METHOD if_oo_adt_classrun~main.
    me->fill_loctn(  ).
  ENDMETHOD.


  METHOD fill_loctn.
    DATA it_loctn TYPE TABLE OF zkat1_d_loctn_a.

    it_loctn = VALUE #(
             ( postcode ='222104' spras ='E' loc_id ='200001' country ='Belarus' city ='Minsk' street ='Dolgobrodskaya' build ='5'  )
             ( postcode ='113800' spras ='E' loc_id ='200002' country ='Belarus' city ='Nesvig' street ='Minsk' build ='61'  )
             ( postcode ='100224' spras ='E' loc_id ='200003' country ='Russia' city ='Kaliningrad' street ='Printed' build ='56'  )
             ( postcode ='200873' spras ='E' loc_id ='200004' country ='Belarus' city ='Nesvig' street ='Parkovaia' build ='32'  )
             ( postcode ='300221' spras ='E' loc_id ='200005' country ='Belarus' city ='Borisov' street ='Mroia' build ='2'  )
             ( postcode ='498222' spras ='E' loc_id ='200006' country ='Belarus' city ='Gomel' street ='Polevaia' build ='17'  )
             ( postcode ='220000' spras ='E' loc_id ='200007' country ='Belarus' city ='Ozero' street ='Zviozdna' build ='325'  )
             ( postcode ='543100' spras ='E' loc_id ='200008' country ='Belarus' city ='Hrodna' street ='Mira' build ='124'  )
             ( postcode ='100200' spras ='E' loc_id ='200009' country ='Poland' city ='Warszawa' street ='Masovieski' build ='75'  )
             ( postcode ='777000' spras ='E' loc_id ='200011' country ='Brasil' city ='Brasilia' street ='Via oeste' build ='787'  ) ).



    DELETE FROM zkat1_d_loctn_a.
    INSERT zkat1_d_loctn_a FROM TABLE @it_loctn.

  ENDMETHOD.
ENDCLASS.
