@EndUserText.label: 'Material group int view'
@AccessControl.authorizationCheck: #NOT_REQUIRED
define view entity ZKAT1_I_MTGROUP
  as select from zkat1_d_mtlgr_t
{
  key spras       as Spras,
  key mtlgr_id    as MtlGrCode,
      description as MtlGrCodeDesc
}
where
  spras = $session.system_language
