@EndUserText.label: 'Document status int view'
@AccessControl.authorizationCheck: #NOT_REQUIRED
define view entity ZKAT1_I_STATUS
  as select from zkat1_d_status_t
{
//  key spras     as Spras,
  key stat_id   as StatCode,
      stat_text as StatCodeDesc     
}
where
  spras = $session.system_language
