CLASS zcm_kat1_msg DEFINITION
  PUBLIC
  INHERITING FROM cx_static_check
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.

    INTERFACES if_abap_behv_message .
    INTERFACES if_t100_message .
    INTERFACES if_t100_dyn_msg .
    CONSTANTS:
      BEGIN OF wrhtype_unknown,
        msgid TYPE symsgid VALUE 'ZKAT1_RAP_MSG',
        msgno TYPE symsgno VALUE '001',
        attr1 TYPE scx_attrname VALUE '',
        attr2 TYPE scx_attrname VALUE '',
        attr3 TYPE scx_attrname VALUE '',
        attr4 TYPE scx_attrname VALUE '',
      END OF wrhtype_unknown.
    CONSTANTS:
      BEGIN OF employee_unknown,
        msgid TYPE symsgid VALUE 'ZKAT1_RAP_MSG',
        msgno TYPE symsgno VALUE '002',
        attr1 TYPE scx_attrname VALUE 'EMPLOYEE',
        attr2 TYPE scx_attrname VALUE '',
        attr3 TYPE scx_attrname VALUE '',
        attr4 TYPE scx_attrname VALUE '',
      END OF employee_unknown .
    CONSTANTS:
      BEGIN OF location_unknown,
        msgid TYPE symsgid VALUE 'ZKAT1_RAP_MSG',
        msgno TYPE symsgno VALUE '003',
        attr1 TYPE scx_attrname VALUE 'LOCATION',
        attr2 TYPE scx_attrname VALUE '',
        attr3 TYPE scx_attrname VALUE '',
        attr4 TYPE scx_attrname VALUE '',
      END OF location_unknown .
    CONSTANTS:
      BEGIN OF number_duplicate,
        msgid TYPE symsgid VALUE 'ZKAT1_RAP_MSG',
        msgno TYPE symsgno VALUE '004',
        attr1 TYPE scx_attrname VALUE 'ARRVNUMBER',
        attr2 TYPE scx_attrname VALUE '',
        attr3 TYPE scx_attrname VALUE '',
        attr4 TYPE scx_attrname VALUE '',
      END OF number_duplicate .
    CONSTANTS:
      BEGIN OF arrvheader_empty,
        msgid TYPE symsgid VALUE 'ZKAT1_RAP_MSG',
        msgno TYPE symsgno VALUE '005',
        attr1 TYPE scx_attrname VALUE 'FIELD',
        attr2 TYPE scx_attrname VALUE '',
        attr3 TYPE scx_attrname VALUE '',
        attr4 TYPE scx_attrname VALUE '',
      END OF arrvheader_empty .

    METHODS constructor
      IMPORTING
        severity   TYPE if_abap_behv_message=>t_severity DEFAULT if_abap_behv_message=>severity-error
        textid     LIKE if_t100_message=>t100key OPTIONAL
        previous   LIKE previous OPTIONAL
        employee   TYPE zkat1_empl_id OPTIONAL
        location   TYPE zkat1_loc_id OPTIONAL
        arrvnumber TYPE zkat1_arrv_id OPTIONAL
        field      TYPE string OPTIONAL .

    DATA employee   TYPE zkat1_empl_id READ-ONLY.
    DATA location   TYPE zkat1_loc_id READ-ONLY.
    DATA arrvnumber TYPE zkat1_arrv_id READ-ONLY.
    DATA field      TYPE string READ-ONLY.
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS ZCM_KAT1_MSG IMPLEMENTATION.


  METHOD constructor ##ADT_SUPPRESS_GENERATION.
    CALL METHOD super->constructor
      EXPORTING
        previous = previous.
    CLEAR me->textid.
    IF textid IS INITIAL.
      if_t100_message~t100key = if_t100_message=>default_textid.
    ELSE.
      if_t100_message~t100key = textid.
    ENDIF.

    me->if_abap_behv_message~m_severity = severity.

    me->employee = employee.
    me->location = location.
    me->arrvnumber = arrvnumber.

  ENDMETHOD.
ENDCLASS.
