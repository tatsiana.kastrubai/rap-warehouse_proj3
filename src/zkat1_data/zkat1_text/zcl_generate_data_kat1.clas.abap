CLASS zcl_generate_data_kat1 DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    INTERFACES if_oo_adt_classrun.
  PROTECTED SECTION.
  PRIVATE SECTION.
    METHODS fill_directory.
ENDCLASS.



CLASS ZCL_GENERATE_DATA_KAT1 IMPLEMENTATION.


  METHOD if_oo_adt_classrun~main.
    me->fill_directory(  ).
  ENDMETHOD.


  METHOD fill_directory.
    DATA it_status_t TYPE TABLE OF zkat1_d_status_t.
    DATA it_doctyp_t TYPE TABLE OF zkat1_d_doctyp_t.
    DATA it_mtlgr_t TYPE TABLE OF zkat1_d_mtlgr_t.

    DATA it_loctn TYPE TABLE OF zkat1_d_loctn_a.

    it_status_t = VALUE #(
              ( spras  = 'E' stat_id = '1' stat_text = 'Expected' )
              ( spras  = 'E' stat_id = '2' stat_text = 'Credited' )
              ( spras  = 'E' stat_id = '3' stat_text = 'Returns' )
              ( spras  = 'R' stat_id = '1' stat_text = 'Ожидание' )
              ( spras  = 'R' stat_id = '2' stat_text = 'Проведен' )
              ( spras  = 'R' stat_id = '3' stat_text = 'Возврат' ) ).

    it_doctyp_t = VALUE #( ( spras ='R' doct_id ='INV' doct_text ='Накладная'  )
                 ( spras ='R' doct_id ='ORD' doct_text ='Заказ'  )
                 ( spras ='R' doct_id ='ACT' doct_text ='Акт'  )
                 ( spras ='E' doct_id ='ACT' doct_text ='Act'  )
                 ( spras ='E' doct_id ='INV' doct_text ='Invoice'  )
                 ( spras ='E' doct_id ='ORD' doct_text ='Order'  ) ).

    it_mtlgr_t = VALUE #( ( spras ='R' mtlgr_id ='900001' description ='Прочие материалы'  )
                     ( spras ='E' mtlgr_id ='900001' description ='Other materials'  )
                     ( spras ='E' mtlgr_id ='100010' description ='Engine'  )
                     ( spras ='E' mtlgr_id ='100011' description ='Sypply system'  )
                     ( spras ='E' mtlgr_id ='100012' description ='Cooling system'  )
                     ( spras ='E' mtlgr_id ='100014' description ='Lubrication system'  )
                     ( spras ='E' mtlgr_id ='100016' description ='Clutch'  )
                     ( spras ='E' mtlgr_id ='100023' description ='Front axle'  )
                     ( spras ='E' mtlgr_id ='100024' description ='Rear axle'  )
                     ( spras ='E' mtlgr_id ='100028' description ='Frame'  )
                     ( spras ='E' mtlgr_id ='100030' description ='Streering control'  )
                     ( spras ='E' mtlgr_id ='100031' description ='Wheels and hubs'  )
                     ( spras ='E' mtlgr_id ='100035' description ='Braking system'  )
                     ( spras ='E' mtlgr_id ='100037' description ='Electrical equipment'  )
                     ( spras ='E' mtlgr_id ='100042' description ='Power take-of'  )
                     ( spras ='R' mtlgr_id ='100010' description ='Двигатель'  )
                     ( spras ='R' mtlgr_id ='100011' description ='Система питания'  )
                     ( spras ='R' mtlgr_id ='100012' description ='Система охлаждения'  )
                     ( spras ='R' mtlgr_id ='100014' description ='Система смазки'  )
                     ( spras ='R' mtlgr_id ='100016' description ='Сцепление'  )
                     ( spras ='R' mtlgr_id ='100023' description ='Мост передний'  )
                     ( spras ='R' mtlgr_id ='100024' description ='Мост задний'  )
                     ( spras ='R' mtlgr_id ='100028' description ='Рама'  )
                     ( spras ='R' mtlgr_id ='100030' description ='Управление рулевое'  )
                     ( spras ='R' mtlgr_id ='100031' description ='Колеса и ступицы'  )
                     ( spras ='R' mtlgr_id ='100035' description ='Система тормозная'  )
                     ( spras ='R' mtlgr_id ='100037' description ='Электрооборудование'  )
                     ( spras ='R' mtlgr_id ='100042' description ='Коробка отбора мощн.'  ) ).

    DELETE FROM zkat1_d_mtlgr_t.
    INSERT zkat1_d_mtlgr_t FROM TABLE @it_mtlgr_t.

    DELETE FROM zkat1_d_status_t.
    INSERT zkat1_d_status_t FROM TABLE @it_status_t.

    DELETE FROM zkat1_d_doctyp_t.
    INSERT zkat1_d_doctyp_t FROM TABLE @it_doctyp_t.

  ENDMETHOD.
ENDCLASS.
