@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Document type int view'
define view entity ZKAT1_I_DOCTYPE
  as select from zkat1_d_doctyp_t
{
//  key spras     as Spras,
  key doct_id   as DocCode,
      doct_text as DocCodeDesc
}
where
  spras = $session.system_language
