@EndUserText.label: 'Wrhous type int view'
@AccessControl.authorizationCheck: #NOT_REQUIRED
define view entity ZKAT1_I_WRHTYPE2
  as select from zkat1_d_wrhtyp_t
{
//  key spras     as Spras,
  key wrhtype   as WrhType,
      type_text as TypeDescr
//      stor_pref as StorPref,
//      max_size  as MaxSize     
}
where
  spras = $session.system_language
